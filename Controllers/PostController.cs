﻿using Microsoft.AspNetCore.Mvc;
using Oblitas_Tafur_Eliana_T1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Oblitas_Tafur_Eliana_T1.Controllers
{
    public class PostController : Controller
    {
        private readonly Examen_T1_Contexto context;

        public PostController(Examen_T1_Contexto context)
        {
            this.context = context;
        }
        [HttpGet]
        public ActionResult Index()
        {
            var post = context.Posts.OrderByDescending(o => o.Fecha_Creacion).ToList();
            return View(post);
        }
        [HttpPost]
        public ActionResult Registrar(Post_clase post)
        {
            post.Fecha_Creacion = DateTime.Now;
            context.Posts.Add(post);
            context.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Registrar()
        {
            return View("Registrar", new Post_clase());
        }
        [HttpGet]
        public ActionResult Detalle(int id)
        {
            var post = context.Posts.Where(o => o.Id == id).FirstOrDefault();
            ViewBag.Comentario = context.Comentarios.
                Where(o => o.IdPost == id).
                OrderByDescending(o => o.FechaComentario).
                ToList();
            return View(post);
        }
        [HttpPost]
        public ActionResult RegistrarComentario(Comentario comentario)
        {
            comentario.FechaComentario = DateTime.Now;
            context.Add(comentario);
            context.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult RegistrarComentario(int id)
        {
            ViewBag.Id = id;
            return View("RegistrarComentario", new Comentario());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Oblitas_Tafur_Eliana_T1.Models
{
    public class Post_clase
    {
        public int Id { get; set; }
        public String Nombre { get; set; }
        public String Autor { get; set; }

        public String Contenido { get; set; }
        public DateTime Fecha_Creacion { get; set; }
        public List<Comentario> Comentario { get; set; }
    }
}

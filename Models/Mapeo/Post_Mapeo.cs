﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Oblitas_Tafur_Eliana_T1.Models.Mapeo
{

    public class Post_Mapeo :IEntityTypeConfiguration<Post_clase>
    {
 

        public void Configure(EntityTypeBuilder<Post_clase> builder)
        {
            builder.ToTable("Post_clase");
            builder.HasKey(o => o.Id);

            builder.HasMany(o => o.Comentario).
                WithOne().
                HasForeignKey(o => o.IdPost);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Oblitas_Tafur_Eliana_T1.Models.Mapeo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Oblitas_Tafur_Eliana_T1.Models
{

    public class Examen_T1_Contexto : DbContext
    {
        public DbSet<Post_clase> Posts { get; set; }
        public DbSet<Comentario> Comentarios { get; set; }
        public Examen_T1_Contexto(DbContextOptions<Examen_T1_Contexto> options)
            : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new Post_Mapeo());
            modelBuilder.ApplyConfiguration(new Comentario_Map());
        }
    }

}
